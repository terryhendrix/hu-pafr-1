/**
 * Created by terryhendrix on 22/02/15.
 */
trait Preparation {
  def handle():Unit
  override def toString = getClass.getSimpleName.replaceAll("\\$$","")
}

object Apron extends Preparation {
  override def handle() = print("puts an apron on")
}

object Knifes extends Preparation {
  override def handle() = print("takes a collection of knifes")
}

object ClassicalMusic extends Preparation {
  override def handle() = print("puts on some classical music")
}

object Beer extends Preparation {
  override def handle() = print("opens a beer")
}

object HardRock extends Preparation {
  override def handle() = print("sets some hardrock on")
}

object RedWine extends Preparation {
  override def handle() = print("haves a glass of red wine")
}

case class Cook(name:String, preparations:List[Preparation] = List.empty) {
  def add(preparation:Preparation) = copy(preparations = preparations :+ preparation)
  def remove(preparation:Preparation) = copy(preparations = preparations.filter(_ != preparation))

  type IndexedPreparation = (Preparation,Int)
  def prepare() = {
    print(name + " ")
    val tail = preparations.reverse.tail.reverse
    tail.zipWithIndex.foreach { indexedPreparation:IndexedPreparation =>
      indexedPreparation._1.handle()
      if(tail.size != indexedPreparation._2 + 1) print(", ")
    }
    if(preparations.size > 1) print(" and ")
    preparations.reverse.head.handle()
    println()
  }
}

case class System(cooks:List[Cook] = List.empty) {
  def add(cook:Cook) = copy(cooks = cooks :+ cook)
  def getCooks = cooks
  def getPreparations = cooks.flatMap(_.preparations).distinct

  def remove(cook:Cook) = copy(cooks = cooks.filter(_ != cook))
  override def toString = cooks.foldLeft(""){(acc,cook) => acc + cook + "\n"}
}

object Main extends App {
  val jeroen = Cook("Jeroen").add(Apron)
  val christian = Cook("Christian").add(Apron).add(Knifes).add(ClassicalMusic)
  val nini = Cook("Nini").add(Beer).add(HardRock)
  val leo = nini.copy(name = "Leo")
  val terry = Cook("Terry").add(RedWine).add(HardRock)
  val system = System().add(jeroen).add(christian).add(nini).add(leo).add(terry)

  println("\n=== Preparing cooks ===")
  system.cooks.foreach(_.prepare())

  println("\n=== All preparations in the system ===")
  println(system.getPreparations)
}
